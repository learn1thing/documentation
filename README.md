# Learn1thing Documentation

##A. Course Apps

### Course is Application based moodle.

[Moodle](https://moodle.org/) is a popular and open-source web-based learning management system (LMS) that is free for anyone to install and use. With Moodle, you can create and deliver learning resources such as courses, readings, and discussion boards to groups of learners. Moodle also allows you to manage user roles, so students and instructors can have different levels of access to materials. Once you install Moodle on your web server, anyone with access to your site can create and participate in browser-based learning.

### Course-installation

Detail of Course Installation [here](course-installation.md)

### Setup plugin

### [**1.User key authentication**](https://moodle.org/plugins/auth_userkey)

Auth plugin for organising simple one way **SSO(single sign on)** between moodle and your external web application. The main idea is to make a web call to moodle and provide one of the possible matching fields to find required user and generate one time login URL. A user can be redirected to this URL to be log in to Moodle without typing username and password.

Log in to Moodle using one time user key based login URL. Auth plugin for organising simple SSO (single sign on) between moodle and your external web application.

###Using

1. Install the plugin as usual. I add function logout,so install this plugin via upload file (.zip).

   ![Screenshot (197)](/img/Screenshot (197).png)

   ​

2. Enable and configure just installed plugin. Set required Mapping field, User key life time, IP restriction and Logout redirect URL. Set Logout redirect URL & URL of SSO Host to `https://store.learn1thing.com/dashboard/login`  to override login page of moodle to redirect login page in middleware.

   ![Screenshot (198)](/img/Screenshot (198).png)

   ​

3. Enable the plugin in Manage Authentication (Dashboard>Site administration>Plugins>Authentication>Manage authentication)

   ![Screenshot (199)](\img\Screenshot (199).png)

   ​

4. Enable web service advance feature (Admin > Advanced features), more info <http://docs.moodle.org/en/Web_services>. And Make sure the web services has been activated/enabled.

   ![Screenshot (200)](\img\Screenshot (200).png)

   ​

5. Enable one of the supported protocols (Dashboard>Site administration>Plugins>Web services>Manage protocols)

   ![Screenshot (201)](\img\Screenshot (201).png)

6. Create a token for a specific user and for the service 'User key authentication web service (Dashboard>Site administration>Plugins>Web services>Manage tokens)

   ![Screenshot (202)](\img\Screenshot (202).png)

   Click add to create token.

   ![Screenshot (203)](\img\Screenshot (203).png)

   Set User field,Service name. you can set IP restriction for enable specific ip can access this web services and you can set expired time for this web services. Then Save Changes.

   ![Screenshot (204)](img\Screenshot (204).png)

   ​

7. Make sure that the "web service" user has 'auth/userkey:generatekey' capability.

8. Configure your external application to make a web call to get login URL.

9. Please navigate to API documentation to get full description for `auth_userkey_request_login_url` function. e.g.

    **Arguments**
   **user (Required)**

   ```
   General structure

   object {
   email string   //A valid email address
   } 

   XML-RPC (PHP structure)

   [user] =>
       Array 
           (
           [email] => string        
           )
           
   REST (POST parameters)

   user[email]= string
   ```

   **Response**

   ```
   General structure

   object {
   loginurl string   //Login URL for a user to log in
   } 

   XML-RPC (PHP structure)
       Array 
           (
           [loginurl] => string        
           )
           
   REST
   <?xml version="1.0" encoding="UTF-8" ?>
   <RESPONSE>
       <SINGLE>
           <KEY name="loginurl">
               <VALUE>string</VALUE>
           </KEY>
       </SINGLE>
   </RESPONSE>
   ```

   ​

------

### 2.Profiler Plugin

Profiler is plugin for online learning that make for survey purpose. 

### Using

1. Install the plugin as usual. this plugin didn`t found in plugin moodle. this plugin is plugin own plugin,so install this plugin via upload file (coursetoprofiler.zip).

   ![Screenshot (197)](img\Screenshot (197).png)



2. Then will be show up validation form. You can continue if validation successful.

   ![Screenshot (205)](img\Screenshot (205).png)


3. Upgrade database, because this plugin create a table for store the profiler from middleware.

   ![Screenshot (206)](img\Screenshot (206).png)



4. Now the profiler plugin already for use. You can add profiler in your course.