

#Course Installation

### Prerequisites

Before you begin this guide you'll need the following:

- A 1GB Ubuntu 16.04 server with a minimum of 200MB of disk space for the Moodle code and as much as you need to store your content. Moodle requires 512MB of memory, but recommends at least 1GB for best performance.
- A non-root user with sudo privileges and a firewall, which you can set up by following [the Ubuntu 16.04 initial server setup guide](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04).
- The LAMP stack (Apache, MySQL, and PHP) installed by following [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04). Be sure to make a note of the root MySQL password you set during this process.

### Step 1 — Install Moodle and Dependencies

Moodle relies on a few pieces of software, including a spell-checking library and a graphing library. Moodle is a PHP application, and it has a few additional PHP library dependencies as well. Before we install Moodle, let's install all of the prerequisite libraries using the package manager. First, ensure you have the latest list of packages:

```
sudo apt-get update

```

Then install Moodle's dependencies:

```
sudo apt-get install aspell graphviz php7.0-curl php7.0-gd php7.0-intl php7.0-ldap php7.0-mysql
```

Next, restart the Apache web server to load the modules you just installed:

```
sudo systemctl restart apache2
```

Download and verify that the course(moodle) directory in your servers web root directory.

Now we need to create a directory outside the web root for Moodle to store all the course-related data that will be stored on the server, but not in the database. It is more secure to create this directory outside the web root so that it cannot be accessed directly from a browser. Execute this command:

```
sudo mkdir /var/moodledata

```

Then set its ownership to make sure that the web service user `www-data` can access the directory:

```
sudo chown -R www-data /var/moodledata

```

Then change the permissions on the folder so that only the owner has full permissions:

```
sudo chmod -R 0770 /var/moodledata

```

Now that you’ve got Moodle on your server, it’s time to set up the database it'll use.

### Step 2 — Configuring the Database

We need to create the MySQL database where Moodle will store most of its data. We'll create the structure that the Moodle code expects, and we'll create a user that Moodle will use to connect to the database.

### Step 3 — Configuring Moodle in the Browser

To finish configuring Moodle, we'll bring up the site in a web browser and provide it with some additional configuration details. In order for the web server to save the configuration, we need to temporarily alter the permission for the Moodle web root.

**Warning:** 
The permissions open this folder up to everyone. If you are uncomfortable with doing this, simply don't change the permission. The web interface will provide instructions for you to manually modify the configuration file.If you do change the permissions, it is very important to undo this as soon as you have completed the setup. That step is included in this tutorial.

```
sudo chmod -R 777 /var/www/html/moodle

```

Now open up a browser and go to `http://your_server_ip/moodle`. You’ll see a page like the following.

![Initial Moodle Setup Screen](https://assets.digitalocean.com/articles/moodle_ubuntu_1604/TpIkbFk.png)

Follow these steps to configure Moodle:

1. Set the language you want to use and click **Next**.
2. On the next screen, set the **Data Directory** to `/var/moodledata` and click **Next**.
3. On the the **Choose Database Driver** page, set **Database driver** to **Improved MySQL (native mysqli)**. Then click **Next**.
4. On the **Database setting** page, enter the username and password for the Moodle MySQL user you created in Step 3. The other fields can be left as they are. Click **Next** to continue.
5. Review the license agreement and confirm that you agree to its terms by pressing **Continue.**
6. Review the **Server Checks** page for any possible issues. Ensure the message "Your server environment meets all minimum requirements" exists at the bottom and press **Continue.**
7. Moodle will install several components, displaying "Success" messages for each. Scroll to the bottom and press **Continue.**
8. You'll then see a page where you can set up your administrator account for Moodle.
   1. For **Username**, enter anything you'd like, ar accept the default.
   2. For **Choose an authentication method**, leave the default value in place.
   3. For **New password**, enter the password you'd like to use.
   4. For **Email**, enter your email address.
   5. Set the rest of the fields to appropriate values.
   6. Click **Update profile**.
9. On the **Front Page Settings** screen, fill in the **Full site name**, the **Short name for site**, set a location, and select whether you want to allow self-registration via email. Then click **Save changes.**

Once you’ve done this. you’ll be taken to the dashboard of your new Moodle installation, logged in as the **admin** user.

Now that your setup is complete, it’s important to restrict permissions to the Moodle web root again. Back in your terminal, execute the following command:

`sudo chmod -R 0755 /var/www/html/moodle`

`(source:https://www.digitalocean.com/community/tutorials/how-to-install-moodle-on-ubuntu-16-04)`

Now your course(moodle) ready to use.



